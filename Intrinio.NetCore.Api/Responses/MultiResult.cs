using Newtonsoft.Json;
using System.Collections.Generic;

namespace Intrinio.NetCore.Responses
{
    public class MultiResult<T>
    {

        [JsonProperty("result_count")]
        public int ResultCount;

        [JsonProperty("page_size")]
        public int PageSize;

        [JsonProperty("current_page")]
        public int CurrentPage;

        [JsonProperty("total_pages")]
        public int TotalPages;

        [JsonProperty("api_call_credits")]
        public int ApiCallCredits;

        [JsonProperty("data")]
        public List<T> Data;
        
    }
}