using System.Collections.Generic;
using Newtonsoft.Json;

namespace Intrinio.NetCore.Responses
{
    public class ErrorResponses
    {
        [JsonProperty("errors")]
        public List<ErrorResponse> Errors;
    }
}