using System.Threading.Tasks;
using Intrinio.NetCore.Api.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Intrinio.NetCore.Extensions;
using Intrinio.NetCore.Data.Models.Public_Companies;
using Intrinio.NetCore.Data.Models.Enums;
using Intrinio.NetCore.Data.Extensions;
using Intrinio.NetCore.Exceptions;
using Intrinio.NetCore.Responses;

namespace Intrinio.NetCore.Api.Api
{
    internal partial class IntrinioApi : IPublicCompanyDataFeed
    {

        private const string CompanyEndpoint = "companies";
        private const string SecuritiesEndpoint = "securities";
        private const string IndicesEndpoint = "indices";
        private const string DataPointEndpoint = "data_point";
        private const string HistoricalEndpoint = "historical_data";
        private const string PricesEndpoint = "prices";
        private const string FinancialsEndpoint = "financials";
        private const string FundamentalsEndpoint = "fundamentals";
        private const string NewsEndpoint = "news";
        private const string TagsEndpoint = "tags";

        public Task<MultiResult<SECFiling>> GetSECFilings(string symbol, ReportType? type = ReportType._4, DateTime? startDate = null, DateTime? endDate = null, int? pageSize = 250, int? pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", symbol);
            queryParams.Add("report_type", type.GetAlternativeValue());
            queryParams.AddIfHasValue("start_date", startDate);
            queryParams.AddIfHasValue("end_date", endDate);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<SECFiling>($"{CompanyEndpoint}/filings", queryParams);
        }

        public Task<MultiResult<Company>> GetCompanies(DateTime? latestFilingDate = null, int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);
            queryParams.AddIfHasValue("latest_filing_date", latestFilingDate);

            return GetMultiResultAsync<Company>(CompanyEndpoint, queryParams);
        }

        public Task<Company> GetCompany(string symbol)
        {
            return GetAsync<Company>(CompanyEndpoint, new { identifier = symbol });
        }

        public Task<MultiResult<CompanyNews>> GetCompanyNews(string identifier, int pageSize = 250, int pageNumber = 1)
        {
            return GetMultiResultAsync<CompanyNews>(NewsEndpoint, 
                new { identifier = identifier, page_size = pageSize, page_number = pageNumber });
        }

        public Task<DataPoint> GetDataPoint(string identifier, string item)
        {
            return GetAsync<DataPoint>(DataPointEndpoint, new { identifier = identifier, item = item });
        }

        public Task<DataPoint> GetDataPoint(string identifier, Tag item)
        {
            return GetDataPoint(identifier, item.GetAlternativeValue());
        }

        public Task<MultiResult<DataPoint>> GetDataPointFromIdentifiers(string item, params string[] identifiers)
        {
            return GetDataPoints(identifiers.AsEnumerable(), new List<string> { item });
        }

        public Task<MultiResult<DataPoint>> GetDataPointFromIdentifiers(Tag item, params string[] identifiers)
        {
            return GetDataPoints(identifiers.AsEnumerable(), new List<string> { item.GetAlternativeValue() });
        }

        public Task<MultiResult<DataPoint>> GetDataPoints(IEnumerable<string> identifiers, IEnumerable<string> items)
        {
            if (identifiers.Count() * items.Count() > 150)
            {
                throw new IntrinioException("Identifier and item count exceeds 150. You may only request up to 150 identifiers/item combinations.");
            }

            return GetMultiResultAsync<DataPoint>(DataPointEndpoint, new { identifier = identifiers.ToCsv(), item = items.ToCsv() });
        }

        public Task<MultiResult<DataPoint>> GetDataPoints(string identifier, params string[] items)
        {
            return GetDataPoints(new List<string> { identifier }, items.AsEnumerable());
        }

        public Task<MultiResult<DataPoint>> GetDataPoints(string identifier, params Tag[] items)
        {
            return GetDataPoints(new List<string> { identifier }, items.Select(x => x.GetAlternativeValue()));
        }

        public Task<MultiResult<HistoricalData>> GetHistoricalData(string identifier, string item, DateTime? startDate = null, DateTime? endDate = null, 
            Frequency frequency = Frequency.yearly, PeriodType type = PeriodType.TTM, SortOrder sortOrder = SortOrder.desc, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", identifier);
            queryParams.Add("item", item);
            queryParams.AddIfHasValue("start_date", startDate);
            queryParams.AddIfHasValue("end_date", endDate);
            queryParams.AddIfHasValue("frequency", frequency);
            queryParams.AddIfHasValue("type", type);
            queryParams.AddIfHasValue("sort_order", sortOrder);
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<HistoricalData>(HistoricalEndpoint, queryParams);
        }

        public Task<MultiResult<HistoricalData>> GetHistoricalData(string identifier, Tag item, DateTime? startDate = null, DateTime? endDate = null,
            Frequency frequency = Frequency.yearly, PeriodType type = PeriodType.TTM, SortOrder sortOrder = SortOrder.desc, int? pageSize = null, int? pageNumber = null)
        {
            return GetHistoricalData(identifier, item.GetAlternativeValue(), startDate, endDate, frequency, type, sortOrder, pageSize, pageNumber);
        }

        public Task<MultiResult<Price>> GetPrices(string identifier, DateTime? startDate = null, DateTime? endDate = null, Frequency frequency = Frequency.yearly, 
            SortOrder sortOrder = SortOrder.desc, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", identifier);
            queryParams.AddIfHasValue("start_date", startDate);
            queryParams.AddIfHasValue("end_date", endDate);
            queryParams.AddIfHasValue("frequency", frequency);
            queryParams.AddIfHasValue("sort_order", sortOrder);
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<Price>(PricesEndpoint, queryParams);
        }

        public Task<MultiResult<Financial>> GetStandardizedFinancials(string identifier, Statement statement, DateTime fiscalYear, 
            FiscalPeriod period, PeriodType? type = null, DateTime? date = null, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", identifier);
            queryParams.Add("statement", statement);
            queryParams.Add("fiscal_year", fiscalYear.ToString("yyyy"));
            queryParams.Add("fiscal_period", period);
            queryParams.AddIfHasValue("type", type);
            queryParams.AddIfHasValue("date", date);
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<Financial>($"{FinancialsEndpoint}/standardized", queryParams);
        }

        public Task<MultiResult<ExchangePrice>> GetExchangePrices(string exchange, DateTime priceDate, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", exchange);
            queryParams.Add("price_date", priceDate.ToString("yyyy-MM-dd"));
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<ExchangePrice>($"{PricesEndpoint}/exchange", queryParams);
        }

        public Task<MultiResult<ExchangePrice>> GetExchangePrices(StockExchangeEnum exchange, DateTime priceDate, int? pageSize = null, int? pageNumber = null)
        {
            return GetExchangePrices(exchange.GetAlternativeValue(), priceDate, pageSize, pageNumber);
        }

        public Task<MultiResult<dynamic>> SearchSecurities(string conditions, SortOrder orderDirection = SortOrder.asc, int pageSize = 250, int pageNumber = 1)
        {
            return GetMultiResultAsync<dynamic>($"{SecuritiesEndpoint}/search",
                new { conditions = CleanConditions(conditions), order_direction = orderDirection, page_size = pageSize, page_number = pageNumber });
        }

        private string CleanConditions(string conditions)
        {
            var splitConditions = conditions.Split(',');
            for(var i = 0; i < splitConditions.Length; i++)
            {
                splitConditions[i] = splitConditions[i].Trim().Replace(" ", "~").Replace(">", "gt")
                    .Replace(">=", "gte").Replace("<", "lt").Replace("<=", "lte");
            }
            return string.Join(",", splitConditions);
        }

        public Task<MultiResult<Security>> GetSecuritiesWithQuery(string query, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1)
        {
            return GetSecuritiesInternal(query, null, lastCrspAdjDate, pageSize, pageNumber);
        }

        public Task<MultiResult<Security>> GetSecuritiesWithExchange(StockExchangeEnum? exchangeSymbol, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1)
        {
            return GetSecuritiesInternal(null, exchangeSymbol.GetAlternativeValue(), lastCrspAdjDate, pageSize, pageNumber);
        }

        public Task<MultiResult<Security>> GetSecuritiesWithExchange(string exchangeSymbol, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1)
        {
            return GetSecuritiesInternal(null, exchangeSymbol, lastCrspAdjDate, pageSize, pageNumber);
        }

        public Task<MultiResult<Security>> GetAllSecurities(int pageSize = 250, int pageNumber = 1)
        {
            return GetSecuritiesInternal(pageSize: pageSize, pageNumber: pageNumber);
        }

        private Task<MultiResult<Security>> GetSecuritiesInternal(string query = null, string exchangeSymbol = null, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            if(!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(exchangeSymbol))
            {
                throw new IntrinioException("Query and exchange symbol cannot both be populated for this request.");
            }

            queryParams.AddIfHasValue("query", query);
            queryParams.AddIfHasValue("exch_symbol", exchangeSymbol);
            queryParams.AddIfHasValue("last_crsp_adj_date", lastCrspAdjDate);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<Security>(SecuritiesEndpoint, queryParams);
        }

        public Task<Security> GetSecurity(string identifier)
        {
            return GetAsync<Security>(SecuritiesEndpoint, new { identifier = identifier });
        }

        public Task<MultiResult<Fundamental>> GetStandardizedFundamentals(string identifier, Statement statement, PeriodType type = PeriodType.FY, DateTime? date = default(DateTime?), int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", identifier);
            queryParams.Add("statement", statement);
            queryParams.Add("type", type);
            queryParams.AddIfHasValue("date", date);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<Fundamental>($"{FundamentalsEndpoint}/standardized", queryParams);
        }

        public Task<MultiResult<TagLabel>> GetStandardizedTagsAndLabels(Statement statement, string ticker = null, Template? template = null, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("statement", statement);
            queryParams.AddIfHasValue("identifier", ticker);
            queryParams.AddIfHasValue("template", template);
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<TagLabel>($"{TagsEndpoint}/standardized", queryParams);
        }

        public Task<MultiResult<Index>> GetAllIndices(string type = null, int pageSize = 250, int pageNumber = 1)
        {
            return GetIndicesInternal(null, null, type, pageSize, pageNumber);
        }

        public Task<MultiResult<Index>> GetIndicesByQuery(string query, string type = null, int pageSize = 250, int pageNumber = 1)
        {
            return GetIndicesInternal(null, query, type, pageSize, pageNumber);
        }

        private Task<MultiResult<Index>> GetIndicesInternal(string identifier = null, string query = null, string type = null, int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.AddIfHasValue("identifier", identifier);
            queryParams.AddIfHasValue("query", query);
            queryParams.AddIfHasValue("type", type);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<Index>(IndicesEndpoint, queryParams);
        }

        public Task<Index> GetIndex(StockIndex index)
        {
            return GetIndex(index.GetAlternativeValue());
        }

        public Task<Index> GetIndex(string identifier)
        {
            return GetAsync<Index>(IndicesEndpoint, new { identifier = identifier });
        }

        public Task<MultiResult<Fundamental>> GetAsReportedFundamentals(string symbol, Statement statement, PeriodType? type = null, DateTime? date = null, int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", symbol);
            queryParams.Add("statement", statement);
            queryParams.AddIfHasValue("type", type);
            queryParams.AddIfHasValue("date", date);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<Fundamental>($"{FundamentalsEndpoint}/reported", queryParams);
        }

        public Task<MultiResult<XBRLTagLabel>> GetAsReportedXBRLTagsAndLabels(string symbol, Statement statement, DateTime fiscalYear, FiscalPeriod fiscalPeriod, PeriodType? type = null, DateTime? date = null, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", symbol);
            queryParams.Add("statement", statement);
            queryParams.Add("fiscal_year", fiscalYear.ToString("yyyy"));
            queryParams.Add("fiscal_period", fiscalPeriod);
            queryParams.AddIfHasValue("type", type);
            queryParams.AddIfHasValue("date", date);
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<XBRLTagLabel>($"{TagsEndpoint}/reported", queryParams);
        }

        public Task<MultiResult<XBRLFinancial>> GetAsReportedFinancials(string symbol, Statement statement, DateTime fiscalYear, FiscalPeriod fiscalPeriod, PeriodType? type = null, DateTime? date = null, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", symbol);
            queryParams.Add("statement", statement);
            queryParams.Add("fiscal_year", fiscalYear.ToString("yyyy"));
            queryParams.Add("fiscal_period", fiscalPeriod);
            queryParams.AddIfHasValue("type", type);
            queryParams.AddIfHasValue("date", date);
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<XBRLFinancial>($"{FinancialsEndpoint}/reported", queryParams);
        }
    }
}