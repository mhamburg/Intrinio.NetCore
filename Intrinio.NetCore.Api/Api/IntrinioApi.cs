using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using Intrinio.NetCore.Exceptions;
using Intrinio.NetCore.Responses;
using Newtonsoft.Json;
using Intrinio.NetCore.Utilities;

namespace Intrinio.NetCore.Api.Api
{
    internal partial class IntrinioApi
    {

        private const string BaseEndpoint = "https://api.intrinio.com";
        private Credentials _credentials;

        public IntrinioApi(Credentials credentials)
        {
            _credentials = credentials;
        }

        protected async Task<T> GetAsync<T>(string urlPath, object queryParams = null)
        {
            var client = BuildClient(urlPath, queryParams);

            try
            {
                return await client.GetJsonAsync<T>();
            }
            catch(FlurlHttpException ex)
            {
                throw await CreateException(ex.Call.Response);
            }
        }

        protected async Task<MultiResult<T>> GetMultiResultAsync<T>(string urlPath, object queryParams = null)
        {
            var client = BuildClient(urlPath, queryParams);

            try
            {
                return await client.GetJsonAsync<MultiResult<T>>();
            }
            catch(FlurlHttpException ex)
            {
                throw await CreateException(ex.Call.Response);
            }
        }

        private async Task<Exception> CreateException(HttpResponseMessage response)
        {
            var url = response.RequestMessage.RequestUri.ToString();
            var content = await response.Content.ReadAsStringAsync();
            var error = JsonConvert.DeserializeObject<ErrorResponses>(content);
            var message = error?.Errors?.FirstOrDefault()?.Human;
            var statusCode = (int) response.StatusCode;

            switch(statusCode)
            {
                case 401:
                    return IntrinioException.Create<IntrinioUnauthorizedException>(message, url, content);
                case 403:
                    return IntrinioException.Create<IntrinioForbiddenException>(message, url, content);
                case 404:
                    return IntrinioException.Create<IntrinioNotFoundException>(message, url, content);
                case 429:
                    return IntrinioException.Create<IntrinioTooManyRequestsException>(message, url, content);
                case 500:
                    return IntrinioException.Create<IntrinioInternalServerErrorException>(message, url, content);
                case 503:
                    return IntrinioException.Create<IntrinioServiceUnavailableException>(message, url, content);
                default:
                    return new IntrinioException(message)
                    {
                        RequestUrl = url,
                        ResponseContent = content,
                        StatusCode = statusCode
                    };
            }
        }

        private IFlurlClient BuildClient(string urlPath, object queryParams)
        {
            return BaseEndpoint.AppendPathSegment(urlPath).SetQueryParams(queryParams)
                .WithBasicAuth(_credentials.Username, _credentials.Password).WithHeader("Accept", "application/json");
        }

    }
}