using System.Threading.Tasks;
using Intrinio.NetCore.Api.Interfaces;
using System.Collections.Generic;
using Intrinio.NetCore.Data.Models.Usage;
using Intrinio.NetCore.Data.Models.Enums;
using Intrinio.NetCore.Responses;

namespace Intrinio.NetCore.Api.Api
{
    internal partial class IntrinioApi : IUsage
    {

        private const string UsagePath = "usage";

        public Task<List<AccessLimits>> GetAccessLimits()
        {
            return GetAsync<List<AccessLimits>>($"{UsagePath}/access");
        }

        public Task<CurrentUsage> GetCurrentUsage(string accessCode)
        {
            return GetAsync<CurrentUsage>($"{UsagePath}/current", new { access_code = accessCode });
        }

        public Task<CurrentUsage> GetCurrentUsage(AccessCode accessCode)
        {
            return GetCurrentUsage(accessCode.ToString());
        }

        public Task<MultiResult<HistoricalUsage>> GetHistoricalUsage(string accessCode)
        {
            return GetMultiResultAsync<HistoricalUsage>($"{UsagePath}/historical", new { access_code = accessCode });
        }

        public Task<MultiResult<HistoricalUsage>> GetHistoricalUsage(AccessCode accessCode)
        {
            return GetHistoricalUsage(accessCode.ToString());
        }
    }
}