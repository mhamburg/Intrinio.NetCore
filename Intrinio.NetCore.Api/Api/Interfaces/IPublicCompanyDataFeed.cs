﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Intrinio.NetCore.Data.Models.Public_Companies;
using Intrinio.NetCore.Data.Models.Enums;
using Intrinio.NetCore.Responses;

namespace Intrinio.NetCore.Api.Interfaces
{
    public interface IPublicCompanyDataFeed
    {
        Task<MultiResult<SECFiling>> GetSECFilings(string symbol, ReportType? type = ReportType._4, DateTime? startDate = null, DateTime? endDate = null, int? pageSize = 250, int? pageNumber = 1);

        Task<MultiResult<Company>> GetCompanies(DateTime? latestFilingDate = null, int pageSize = 250, int page_number = 1);

        Task<Company> GetCompany(string symbol);

        Task<DataPoint> GetDataPoint(string identifier, string item);
        
        Task<DataPoint> GetDataPoint(string identifier, Tag item);

        Task<MultiResult<DataPoint>> GetDataPointFromIdentifiers(string item, params string[] identifiers);

        Task<MultiResult<DataPoint>> GetDataPointFromIdentifiers(Tag item, params string[] identifiers);

        Task<MultiResult<DataPoint>> GetDataPoints(IEnumerable<string> identifiers, IEnumerable<string> items);

        Task<MultiResult<DataPoint>> GetDataPoints(string identifier, params string[] items);

        Task<MultiResult<DataPoint>> GetDataPoints(string identifier, params Tag[] items);

        Task<MultiResult<HistoricalData>> GetHistoricalData(string identifier, string item, DateTime? startDate = null, DateTime? endDate = null, 
            Frequency frequency = Frequency.yearly, PeriodType type = PeriodType.TTM, SortOrder sortOrder = SortOrder.desc, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<HistoricalData>> GetHistoricalData(string identifier, Tag item, DateTime? startDate = null, DateTime? endDate = null,
            Frequency frequency = Frequency.yearly, PeriodType type = PeriodType.TTM, SortOrder sortOrder = SortOrder.desc, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<Price>> GetPrices(string identifier, DateTime? startDate = null, DateTime? endDate = null, Frequency frequency = Frequency.yearly, 
            SortOrder sortOrder = SortOrder.desc, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<CompanyNews>> GetCompanyNews(string identifier, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<ExchangePrice>> GetExchangePrices(string exchange, DateTime priceDate, int? pageSize = null, int? pageNumber = null);
        
        Task<MultiResult<ExchangePrice>> GetExchangePrices(StockExchangeEnum exchange, DateTime priceDate, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<Security>> GetAllSecurities(int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Security>> GetSecuritiesWithQuery(string query, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Security>> GetSecuritiesWithExchange(string exchangeSymbol, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1);
        
        Task<MultiResult<Security>> GetSecuritiesWithExchange(StockExchangeEnum? exchangeSymbol, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1);

        Task<Security> GetSecurity(string identifier);

        Task<MultiResult<dynamic>> SearchSecurities(string conditions, SortOrder orderDirection = SortOrder.asc, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Fundamental>> GetStandardizedFundamentals(string identifier, Statement statement, PeriodType type = PeriodType.FY, DateTime? date = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<TagLabel>> GetStandardizedTagsAndLabels(Statement statement, string ticker = null, Template? template = null, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<Financial>> GetStandardizedFinancials(string identifier, Statement statement, DateTime fiscalYear, FiscalPeriod period,
            PeriodType? type = null, DateTime? date = null, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<Index>> GetAllIndices(string type = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Index>> GetIndicesByQuery(string query, string type = null, int pageSize = 250, int pageNumber = 1);

        Task<Index> GetIndex(string index);

        Task<Index> GetIndex(StockIndex index);

        Task<MultiResult<Fundamental>> GetAsReportedFundamentals(string symbol, Statement statement, PeriodType? type = null, DateTime? date = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<XBRLTagLabel>> GetAsReportedXBRLTagsAndLabels(string symbol, Statement statement, DateTime fiscalYear, FiscalPeriod fiscalPeriod, 
            PeriodType? type = null, DateTime? date = null, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<XBRLFinancial>> GetAsReportedFinancials(string symbol, Statement statement, DateTime fiscalYear, FiscalPeriod fiscalPeriod,
            PeriodType? type = null, DateTime? date = null, int? pageSize = null, int? pageNumber = null);
    }
}
