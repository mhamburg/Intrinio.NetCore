﻿using Intrinio.NetCore.Data.Models.Enums;
using Intrinio.NetCore.Data.Models.Master_Data;
using Intrinio.NetCore.Data.Models.Public_Companies;
using Intrinio.NetCore.Responses;
using System;
using System.Threading.Tasks;

namespace Intrinio.NetCore.Api.Api.Interfaces
{
    public interface IMasterDataFeed
    {

        Task<MultiResult<Company>> GetCompanyMaster(string query = null, DateTime? latestFilingDate = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Security>> GetSecurityMaster(string identifier = null, string query = null, string exchange = null, bool usOnly = false, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Security>> GetSecurityMaster(string identifier = null, string query = null, StockExchangeEnum? exchange = null, bool usOnly = false, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Index>> GetIndexMaster(string identifier, IndexType? type = null, Order? order = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Index>> GetIndexMasterByQuery(string query, IndexType? type = null, Order? order = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Owner>> GetOwnerMaster(int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Owner>> GetOwnerMasterByInstitutional(bool institutional, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Owner>> GetOwnerMasterByIndex(string indexKey, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Owner>> GetOwnerMasterByQuery(string query, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<StockExchange>> GetStockExchangeMaster(string query = null, int pageSize = 250, int pageNumber = 1);

    }
}
