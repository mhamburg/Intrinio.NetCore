﻿using Intrinio.NetCore.Data.Models;
using Intrinio.NetCore.Data.Models.Enums;
using Intrinio.NetCore.Data.Models.Usage;
using Intrinio.NetCore.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Intrinio.NetCore.Api.Interfaces
{
    public interface IUsage
    {

        Task<List<AccessLimits>> GetAccessLimits();

        Task<CurrentUsage> GetCurrentUsage(string accessCode);

        Task<CurrentUsage> GetCurrentUsage(AccessCode accessCode);

        Task<MultiResult<HistoricalUsage>> GetHistoricalUsage(string accessCode);

        Task<MultiResult<HistoricalUsage>> GetHistoricalUsage(AccessCode accessCode);

    }
}
