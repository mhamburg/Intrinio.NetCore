namespace Intrinio.NetCore.Exceptions
{
    public class IntrinioUnauthorizedException : IntrinioException
    {
        public IntrinioUnauthorizedException(string message) : base(message)
        {
            StatusCode = 401;
        }
    }
}