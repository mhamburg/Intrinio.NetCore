namespace Intrinio.NetCore.Exceptions
{
    public class IntrinioForbiddenException : IntrinioException
    {
        public IntrinioForbiddenException(string message) : base(message)
        {
            StatusCode = 403;
        }
    }
}