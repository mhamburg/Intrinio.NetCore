namespace Intrinio.NetCore.Exceptions
{
    public class IntrinioInternalServerErrorException : IntrinioException
    {
        
        public IntrinioInternalServerErrorException(string message) : base(message)
        {
            StatusCode = 500;
        }

    }
}