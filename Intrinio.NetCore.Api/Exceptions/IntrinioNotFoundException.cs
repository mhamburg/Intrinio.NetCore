namespace Intrinio.NetCore.Exceptions
{
    public class IntrinioNotFoundException : IntrinioException
    {
        public IntrinioNotFoundException(string message) : base(message)
        {
            StatusCode = 404;
        }
    }
}