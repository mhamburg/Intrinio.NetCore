namespace Intrinio.NetCore.Exceptions
{
    public class IntrinioServiceUnavailableException : IntrinioException
    {
        
        public IntrinioServiceUnavailableException(string message) : base(message)
        {
            StatusCode = 529;
        }

    }
}