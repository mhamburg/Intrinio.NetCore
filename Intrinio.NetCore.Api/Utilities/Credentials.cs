namespace Intrinio.NetCore.Utilities
{
    public class Credentials
    {
        public string Username;

        public string Password;

        public Credentials(string username, string password)
        {
            Username = username;
            Password = password;
        }
    }
}