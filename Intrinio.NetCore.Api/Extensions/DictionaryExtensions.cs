﻿using System;
using System.Collections.Generic;

namespace Intrinio.NetCore.Extensions
{
    public static class DictionaryExtensions
    {

        public static void AddIfHasValue<T>(this Dictionary<T, object> dict, T key, object value)
        {
            if(value == null)
                return;

            if(value is DateTime)
            {
                dict.Add(key, ((DateTime) value).ToString("yyyy-MM-dd"));
                return;
            }

            dict.Add(key, value);
        }

    }
}
