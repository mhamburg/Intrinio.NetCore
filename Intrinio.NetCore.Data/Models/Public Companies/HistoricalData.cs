﻿using Newtonsoft.Json;
using System;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class HistoricalData
    {

        [JsonProperty("date")]
        public DateTime Date;

        [JsonProperty("value")]
        public dynamic Value;

    }
}
