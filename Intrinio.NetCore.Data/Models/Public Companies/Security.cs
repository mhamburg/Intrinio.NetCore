using System;
using Newtonsoft.Json;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class Security
    {
        [JsonProperty("ticker")]
        public string Ticker;

        [JsonProperty("figi_ticker")]
        public string FigiTicker;

        [JsonProperty("figi")]
        public string Figi;

        [JsonProperty("security_name")]
        public string SecurityName;

        [JsonProperty("market_sector")]
        public string MarketSector;

        [JsonProperty("security_type")]
        public string SecurityType;

        [JsonProperty("stock_exchange")]
        public string StockExchange;

        [JsonProperty("last_crsp_adj_date")]
        public DateTime? LastCrspAdjDate;

        [JsonProperty("composite_figi")]
        public string CompositeFigi;

        [JsonProperty("figi_uniqueid")]
        public string FigiUniqueId;

        [JsonProperty("share_class_figi")]
        public string ShareClassFigi;

        [JsonProperty("figi_exch_cntry")]
        public string FigiExchangeCountry;

        [JsonProperty("currency")]
        public string Currency;

        [JsonProperty("mic")]
        public string MarketIdentificationCode;

        [JsonProperty("exch_symbol")]
        public string ExchangeSymbol;

        [JsonProperty("etf")]
        public bool ETF;

        [JsonProperty("delisted_security")]
        public bool DelistedSecurity;

        [JsonProperty("primary_listing")]
        public bool PrimaryListing;
    }
}