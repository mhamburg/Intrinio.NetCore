using Newtonsoft.Json;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class Index
    {
        [JsonProperty("symbol")]
        public string Symbol;

        [JsonProperty("index_name")]
        public string IndexName;

        [JsonProperty("country")]
        public string Country;

        [JsonProperty("continent")]
        public string Continent;

        [JsonProperty("index_type")]
        public string IndexType;
    }
}