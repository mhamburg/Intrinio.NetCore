using System;
using Newtonsoft.Json;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class CompanyNews
    {
        [JsonProperty("title")]
        public string Title;

        [JsonProperty("publication_date")]
        public DateTime PublicationDate;

        [JsonProperty("url")]
        public string Url;

        [JsonProperty("summary")]
        public string Summary;
    }
}