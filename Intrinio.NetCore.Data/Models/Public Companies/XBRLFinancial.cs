﻿using Newtonsoft.Json;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class XBRLFinancial
    {

        [JsonProperty("xbrl_tag")]
        public string XBRLTag;

        [JsonProperty("domain_tag")]
        public string DomainTag;

        [JsonProperty("value")]
        public dynamic Value;

    }
}
