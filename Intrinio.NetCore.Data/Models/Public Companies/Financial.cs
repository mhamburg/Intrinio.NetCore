﻿using Intrinio.NetCore.Data.Extensions;
using Intrinio.NetCore.Data.Models.Enums;
using Newtonsoft.Json;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class Financial
    {

        [JsonProperty("tag")]
        public string Tag;

        [JsonIgnore]
        public Tag? TagEnum
        {
            get
            {
                if (string.IsNullOrEmpty(Tag))
                    return null;

                return Tag.GetEnumFromMember<Tag>();
            }
        }

        [JsonProperty("value")]
        public dynamic Value;

    }
}
