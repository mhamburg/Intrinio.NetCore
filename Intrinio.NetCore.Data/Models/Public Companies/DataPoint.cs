﻿using Intrinio.NetCore.Data.Extensions;
using Intrinio.NetCore.Data.Models.Enums;
using Newtonsoft.Json;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class DataPoint
    {

        [JsonProperty("identifier")]
        public string Identifier;

        [JsonProperty("item")]
        public string Item;

        [JsonIgnore]
        public Tag? ItemTag
        {
            get
            {
                if (string.IsNullOrEmpty(Item))
                    return null;

                return Item.GetEnumFromMember<Tag>();
            }
        }

        [JsonProperty("value")]
        public dynamic Value;

    }
}
