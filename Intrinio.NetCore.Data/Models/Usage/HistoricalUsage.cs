﻿using Newtonsoft.Json;
using System;

namespace Intrinio.NetCore.Data.Models.Usage
{
    public class HistoricalUsage
    {

        [JsonProperty("date")]
        public DateTime Date;

        //This is only a decimal because I am receiving 14.0 and not 14
        [JsonProperty("calls")]
        public decimal Calls;

    }
}
