﻿using Newtonsoft.Json;

namespace Intrinio.NetCore.Data.Models.Usage
{
    public class CurrentUsage
    {

        [JsonProperty("access_code")]
        public string AccessCode;

        [JsonProperty("current")]
        public int Current;

        [JsonProperty("limit")]
        public int Limit;

        [JsonProperty("percent")]
        public int Percent;

    }
}
