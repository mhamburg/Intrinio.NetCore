﻿using Newtonsoft.Json;

namespace Intrinio.NetCore.Data.Models.Master_Data
{
    public class StockExchange
    {

        [JsonProperty("symbol")]
        public string Symbol;

        [JsonProperty("mic")]
        public string MIC;

        [JsonProperty("institution_name")]
        public string InstitutionName;

        [JsonProperty("acronym")]
        public string Acronym;

        [JsonProperty("city")]
        public string City;

        [JsonProperty("country")]
        public string Country;

        [JsonProperty("country_code")]
        public string CountryCode;

        [JsonProperty("website")]
        public string Website;

    }
}
